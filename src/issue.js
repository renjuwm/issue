/**
 * Created by Renju on 2/17/2016.
 * Functions for CRUD Issue
 */

"use strict";
let Issue = require('./../model/issue.model').model;
let debug = require('debug')('ridgeback.issue');

let createIssue = (newIssue, next) => {
    debug("Issue Create : Called ");
    newIssue.issue_id = undefined;
    return newIssue.save().then(
        (issue) => {
            debug("Issue Create : Saved => " + issue);
            return next(undefined, issue);
        }
    ).catch(error => {
        debug("Issue Create : Exception => " + error);
            next(error, null);
    });
};

let getIssueById = (issueId, next) => {
    debug("Issue getIssueById : Called ");
    return Issue.findOne({issue_id: issueId})
        .then((issue) => {
            debug("Issue getIssueById : found issue => " + issue);
            return next(undefined, issue);
        })
        .catch(error => {
            debug("Issue getIssueById : Exception => " + error);
            next(error, null)
        });
};

/**
 *
 * @param updateIssue
 * @param next
 * @returns {*}
 * @TODO Validate Customer before updating
 * @TODO Business Logic for Status Change
 * @TODO Label Update
 */
let modifyIssue = (updateIssue, next) => {
    debug("Issue modifyIssue : Called ");
    let Issue_id = updateIssue.issue_id;
    if (Issue_id > 0) {
        return Issue
            .findOne({issue_id: Issue_id})
            .then(
                (issue) => {
                    debug("Issue modifyIssue : found issue => " + issue);
                    if (issue === null) {
                        return next({
                            message: {
                                error: "Issue not found"
                            }
                        });
                    }
                    issue.subject = updateIssue.subject || issue.subject;
                    issue.body = updateIssue.body || issue.body;
                    issue.customer.objRef = updateIssue.customer.objRef || issue.customer.objRef;
                    issue.customer.customer_display_name = updateIssue.customer.customer_display_name || issue.customer.customer_display_name;
                    issue.channel.channel_type = updateIssue.channel.channel_type || issue.channel.channel_type;
                    issue.assigned_user.objRef = updateIssue.assigned_user.objRef || issue.assigned_user.objRef;
                    issue.assigned_user.user_display_name = updateIssue.assigned_user.user_display_name || issue.assigned_user.user_display_name;
                    issue.assigned_group.objRef = updateIssue.assigned_group.objRef || issue.assigned_group.objRef;
                    issue.assigned_group.group_name = updateIssue.assigned_group.group_name || issue.assigned_group.group_name;
                    if (updateIssue.status) {
                        issue.status = updateIssue.status;
                        issue.status_changed_at = new Date();
                    }
                    issue.priority = updateIssue.priority || issue.priority;
                    issue.description = updateIssue.description || issue.description;
                    issue.reported_by.objRef = updateIssue.reported_by.objRef || issue.reported_by.objRef;
                    issue.reported_by.user_display_name = updateIssue.reported_by.user_display_name || issue.reported_by.user_display_name;
                    return issue.save()
                        .then((issue) => {
                            debug("Issue modifyIssue : Updated => " + issue);
                            return next(undefined, issue);
                        })
                        .catch((error) => {
                            debug("Issue modifyIssue : Exception => " + error);
                            return next(error, undefined);
                        });
                })
            .catch((error) => {
                debug("Issue modifyIssue : Exception => " + error);
                return next(error);
            });
    } else {
        const error = {
            message: {
                error: "Issue Id is not valid"
            }
        };
        debug("Issue modifyIssue : BadArgument => " + error);
        return next(error);
    }
};

let eraseIssue = (next) => {
    next(new Error("Function not implemented yet"));
};

module.exports = {
    create: createIssue,
    getByIssueId: getIssueById,
    update: modifyIssue
};