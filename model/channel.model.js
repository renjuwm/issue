/****
 * Created by Renju on 2/15/2016.
 */
'use strict';

let mongoose = require("mongoose");

let channelSchema = new mongoose.Schema({});

module.exports = {
    schema: channelSchema,
    model: mongoose.model('Channel', channelSchema)
};
