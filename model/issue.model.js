/****
 * Created by Renju on 2/11/2016.
 * @TODO : Validation before update and save
 */
'use strict';

let mongoose = require('mongoose');
let labelSchema = require('./label.model').schema;
let messageSchema = require('./message.model').schema;
let issueLibSeq = require('./issue-lib-seq.model.js').model;
/*

 */

let issueSchema = new mongoose.Schema({
        issue_id: {
            type: Number,
            unique: true,
            index: true,
            require: true
        },
        subject: {
            type: String,
            required: true,
            trim: true
        },
        body: {
            type: String,
            required: true
        },
        customer: {
            objRef: {
                type: mongoose.Schema.Types.ObjectId
            },
            customer_display_name: {
                type: String
            }
        },
        channel: {
            channel_type: {
                type: String,
                required: true
            }
        },
        assigned_user: {
            objRef: {
                type: mongoose.Schema.Types.ObjectId
            },
            user_display_name: {
                type: String
            }
        },
        assigned_group: {
            objRef: {
                type: mongoose.Schema.Types.ObjectId
            },
            group_name: {
                type: String
            }
        },
        status: {
            type: String,
            required: true
        },
        priority: {
            type: Number,
            required: true
        },
        labels: [labelSchema],
        description: {
            type: String
        },
        created_by: {
            objRef: {
                type: mongoose.Schema.Types.ObjectId
            },
            user_display_name: {
                type: String
            }
        },
        reported_by: {
            objRef: {
                type: mongoose.Schema.Types.ObjectId
            },
            user_display_name: {
                type: String
            }
        },
        status_changed_at: {
            type: Date
        },
        erased_at: {
            type: Date
        },
        messages: [messageSchema]
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    });


/*
 Function will be called before saving model to database
 */
issueSchema.pre('save', function (next) {
    var doc = this;

    // If value already set then dont update Issue Sequence
    if (doc.issue_id !== undefined) {
        next();
    } else {
        // Get the issue_id sequence from Sequence table and update the issue_id
        issueLibSeq
            .where({"_id": "issue_id_seq"})
            .update({$inc: {seq: 1}}, function (error, data) {
                if (error) {
                    return next(error);
                }
                issueLibSeq.findOne({
                    _id: "issue_id_seq"
                }, (err, issue_seq) => {
                    if (err) {
                        return console.error("ErrorMessage: " + err);
                    }
                    doc.issue_id = issue_seq.seq;
                    next();
                });
            });
    }


});

module.exports = {
    schema: issueSchema,
    model: mongoose.model('Issue', issueSchema)
};