/****
 * Created by Renju on 2/15/2016.
 */
'use strict';

let mongoose = require("mongoose");

let labelSchema = new mongoose.Schema({
    label: {
        type: String,
        required: true,
        unique: true
    }
});

module.exports = {
    schema: labelSchema,
    model: mongoose.model('Label', labelSchema)
};
