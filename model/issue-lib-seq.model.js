/****
 * Created by Renju on 2/15/2016.
 */
'use strict';

let mongoose = require("mongoose");

let issueLibSeqSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    seq: {
        type: Number,
        default: 0
    }
});

module.exports = {
    schema: issueLibSeqSchema,
    model: mongoose.model('IssueLibSeq', issueLibSeqSchema)
};
