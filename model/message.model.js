/****
 * Created by Renju on 2/15/2016.
 */
'use strict';

let mongoose = require("mongoose");

let messageSchema = new mongoose.Schema({
    type:   {
        type:   String,
        enum:   ["note","mail"],
        required: true
    },
    subject: {
        type: String
    },
    body: {
        type: String
    },
    from: {
        type: String
    },
    to: {
        type: String
    },
    bcc: {
        type: String
    },
    direction: {
        type: String,
        enum: ["IN", "OUT"]
    },
    created_by: {
        user_id: {
            type: Number
        },
        user_display_name: {
            type: String
        }
    },
    created_at: {
        type: Date
    },
    last_modified_at: {
        type: Date
    },
    erased_at: {
        type: Date
    }
});

module.exports = {
    schema: messageSchema,
    model: mongoose.model('Message', messageSchema)
};
