// Tests for issue features
// @Author : Renju
// When you add new test cases add it in the end
"use strict";
var assert = require('chai').assert;
var test_helper = require('./fixtures/test-helper');
var Issue = require('./../index').issueModel;
var faker = require('faker');
var mongoose = require('mongoose');
var issueLib = require('./../index').issue;
var issueSeq = require('./../model/issue-lib-seq.model').model;

before((done) => {
    test_helper.createDB(() => {
        "use strict";
        done();
        //console.log("  DB Created");
    });
});

after(() => {
    test_helper.dropDB();
    //console.log("  DB drop");
});

//test 1 is equal to 1
describe("Base Test", () => {
    it("1 should be equal to 1", () => {
        assert.strictEqual(1, 1, "These values are strict equal");
    });
});


//test mongoose db
describe("Database", () => {
    "use strict";

    before((done) => {
        test_helper.resetDB(() => {
            done();
            //console.log("  DB Reset");
        });
    });

    it("Mongoose lib should be mocked", (done) => {
        assert.strictEqual(test_helper.isMocked(), true, "Expecting mongoose is mocked");
        done();
    });

    it("Mongoose DB should be ready", () => {
        assert.strictEqual(test_helper.mongooseReadyState(), 1, "ReadyState should be 1");
    });

});

//Testing Issue Library
describe("Issue Lib", () => {
    "use strict";
    let newIssue;
    let issue_id;
    let modifyIssue = new Issue();
    before((done) => {

        test_helper.resetDB(() => {

            var zeroSeq = new issueSeq({
                _id: "issue_id_seq",
                seq: 0
            });
            zeroSeq
                .save()
                .then(
                    () => {
                        try {
                            newIssue = new Issue({
                                issue_id: faker.random.number({
                                    'min': 1,
                                    'max': 10
                                }),
                                subject: faker.lorem.sentence(),
                                body: faker.lorem.paragraph(),
                                customer: {
                                    objRef: mongoose.Types.ObjectId(),
                                    customer_id: 1,
                                    customer_display_name: faker.name.findName()
                                },
                                channel: {
                                    objRef: mongoose.Types.ObjectId(),
                                    channel_type: "web"
                                },
                                assigned_user: {
                                    objRef: mongoose.Types.ObjectId(),
                                    user_id: 101,
                                    user_display_name: faker.name.user_id
                                },
                                assigned_group: {
                                    objRef: mongoose.Types.ObjectId(),
                                    group_id: 10,
                                    group_name: 'group1'
                                },
                                status: "NEW",
                                priority: 1,
                                description: faker.lorem.sentence(),
                                created_by: {
                                    user_id: 101,
                                    user_display_name: faker.name.findName()
                                },
                                reported_by: {
                                    user_id: 1,
                                    user_display_name: faker.name.findName()
                                },
                                status_changed_at: new Date(),
                                created_at: new Date(),
                                labels: [
                                    {
                                        label: faker.lorem.words(1)
                                    },
                                    {
                                        label: faker.lorem.words(1)
                                    }
                                ]
                            });
                            done();
                        } catch (ex) {
                            done(ex);
                        }
                    }
                );
        });

    });

    it("Issue Create Test", (done) => {
        "use strict";

        issueLib.create(newIssue, (error, issue) => {
            try {
                issue_id = issue.issue_id;
                assert.isDefined(issue._id, "_id is required");
                assert.isDefined(issue.issue_id, "issue_id is required");
                assert.strictEqual(issue.subject, newIssue.subject, "subject should equal");
                assert.strictEqual(issue.body, newIssue.body, "body should equal");
                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                assert.strictEqual(issue.customer.objRef, newIssue.customer.objRef, "customer.objRef should equal");
                assert.strictEqual(issue.customer.customer_id, newIssue.customer.customer_id, "customer.customer_id should equal");
                assert.strictEqual(issue.customer.customer_display_name, newIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                assert.strictEqual(issue.channel.channel_type, newIssue.channel.channel_type, "channel.channel_type should equal");
                assert.strictEqual(issue.assigned_user.objRef, newIssue.assigned_user.objRef, "assigned_user.objRef should equal");
                assert.strictEqual(issue.assigned_user.user_id, newIssue.assigned_user.user_id, "assigned_user.user_id should equal");
                assert.strictEqual(issue.assigned_user.user_display_name, newIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                assert.strictEqual(issue.assigned_group.objRef, newIssue.assigned_group.objRef, "assigned_group.objRef should equal");
                assert.strictEqual(issue.assigned_group.group_id, newIssue.assigned_group.group_id, "assigned_group.group_id should equal");
                assert.strictEqual(issue.assigned_group.group_name, newIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                assert.strictEqual(issue.status, newIssue.status, "status should equal");
                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                assert.strictEqual(issue.created_by.user_id, newIssue.created_by.user_id, "created_by.user_id should equal");
                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                assert.strictEqual(issue.reported_by.user_id, newIssue.reported_by.user_id, "reported_by.user_id should equal");
                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                assert.strictEqual(issue.status_changed_at, newIssue.status_changed_at, "status_changed_at should equal");
                assert.strictEqual(issue.created_at, newIssue.created_at, "created_at should equal");
                assert.deepEqual(issue.labels, newIssue.labels, "labels should be equal");
                done();
                //done(new Error("Test Function implementation not yet implemented"));
            }
            catch (exp) {
                done(exp);
            }
        });

    });

    it("Issue Get by Issue ID Test", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                assert.isDefined(issue._id, "_id is required");
                assert.isDefined(issue.issue_id, "issue_id is required");
                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                assert.strictEqual(issue.subject, newIssue.subject, "subject should equal");
                assert.strictEqual(issue.body, newIssue.body, "body should equal");
                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                assert.strictEqual(issue.customer.objRef.toString(), newIssue.customer.objRef.toString(), "customer.objRef should equal");
                assert.strictEqual(issue.customer.customer_id, newIssue.customer.customer_id, "customer.customer_id should equal");
                assert.strictEqual(issue.customer.customer_display_name, newIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                assert.strictEqual(issue.channel.channel_type, newIssue.channel.channel_type, "channel.channel_type should equal");
                assert.strictEqual(issue.assigned_user.objRef.toString(), newIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                assert.strictEqual(issue.assigned_user.user_id, newIssue.assigned_user.user_id, "assigned_user.user_id should equal");
                assert.strictEqual(issue.assigned_user.user_display_name, newIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                assert.strictEqual(issue.assigned_group.objRef.toString(), newIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                assert.strictEqual(issue.assigned_group.group_id, newIssue.assigned_group.group_id, "assigned_group.group_id should equal");
                assert.strictEqual(issue.assigned_group.group_name, newIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                assert.strictEqual(issue.status, newIssue.status, "status should equal");
                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                assert.strictEqual(issue.created_by.user_id, newIssue.created_by.user_id, "created_by.user_id should equal");
                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                assert.strictEqual(issue.reported_by.user_id, newIssue.reported_by.user_id, "reported_by.user_id should equal");
                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                assert.strictEqual(issue.status_changed_at.toString(), newIssue.status_changed_at.toString(), "status_changed_at should equal");
                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                done();
                //done(new Error("Test Function implementation not yet implemented"));
            }
            catch (exp) {
                done(exp);
            }
        });

    });


    it("Issue Update Test : Change Subject and Body", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.subject = faker.lorem.sentence() + " modified1";
                modifyIssue.body = faker.lorem.paragraph() + " modified2";
                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined");
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), newIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_id, newIssue.customer.customer_id, "customer.customer_id should equal");
                                assert.strictEqual(issue.customer.customer_display_name, newIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, newIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), newIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_id, newIssue.assigned_user.user_id, "assigned_user.user_id should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, newIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), newIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_id, newIssue.assigned_group.group_id, "assigned_group.group_id should equal");
                                assert.strictEqual(issue.assigned_group.group_name, newIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, newIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.user_id, newIssue.created_by.user_id, "created_by.user_id should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.user_id, newIssue.reported_by.user_id, "reported_by.user_id should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.strictEqual(issue.status_changed_at.toString(), newIssue.status_changed_at.toString(), "status_changed_at should equal");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });

    it("Issue Update Test : if issue not found return error message", (done) => {
        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = 999999999;
                modifyIssue.subject = issue.subject;
                issueLib.update(modifyIssue, (error) => {
                    try {
                        assert.isDefined(error, "error object is required");
                        assert.strictEqual(error.message.error,"Issue not found","Error Message should be equal");
                        done();
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });

    it("Issue Update Test : Change to valid Customer", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.customer.objRef = mongoose.Types.ObjectId();
                modifyIssue.customer.customer_id = 2;
                modifyIssue.customer.customer_display_name = faker.name.findName();
                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined "+JSON.stringify(error));
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), modifyIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_display_name, modifyIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, newIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), newIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, newIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), newIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_id, newIssue.assigned_group.group_id, "assigned_group.group_id should equal");
                                assert.strictEqual(issue.assigned_group.group_name, newIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, newIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.user_id, newIssue.created_by.user_id, "created_by.user_id should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.user_id, newIssue.reported_by.user_id, "reported_by.user_id should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.strictEqual(issue.status_changed_at.toString(), newIssue.status_changed_at.toString(), "status_changed_at should equal");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });

    it("Issue Update Test : Change Channel", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.channel.channel_type = "EMAIL";
                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined "+JSON.stringify(error));
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), modifyIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_display_name, modifyIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, modifyIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), newIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, newIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), newIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_name, newIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, newIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.objRef, newIssue.created_by.objRef, "created_by.objRef should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.user_id, newIssue.reported_by.user_id, "reported_by.user_id should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.strictEqual(issue.status_changed_at.toString(), newIssue.status_changed_at.toString(), "status_changed_at should equal");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });


    it("Issue Update Test : Assigned user and group", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.assigned_user.objRef = mongoose.Types.ObjectId();
                modifyIssue.assigned_user.user_id = 150;
                modifyIssue.assigned_user.user_display_name = faker.name.user_id;
                modifyIssue.assigned_group.objRef = mongoose.Types.ObjectId();
                modifyIssue.assigned_group.group_id = 20;
                modifyIssue.assigned_group.group_name = "group2";
                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined "+JSON.stringify(error));
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), modifyIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_display_name, modifyIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, modifyIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), modifyIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, modifyIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), modifyIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_name, modifyIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, newIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.objRef, newIssue.created_by.objRef, "created_by.user_id should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.objRef, newIssue.reported_by.objRef, "reported_by.user_id should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.strictEqual(issue.status_changed_at.toString(), newIssue.status_changed_at.toString(), "status_changed_at should equal");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });

    it("Issue Update Test : status", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error,issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.status = "OPEN";
                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined "+JSON.stringify(error));
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), modifyIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_display_name, modifyIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, modifyIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), modifyIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, modifyIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), modifyIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_name, modifyIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, modifyIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, newIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.objRef, newIssue.created_by.objRef, "created_by.objRef should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.objRef, newIssue.reported_by.objRef, "reported_by.objRef should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.isAbove(issue.status_changed_at, newIssue.status_changed_at, "status_changed_at should should greater than older value when status changes");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });

    it("Issue Update Test : priority", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.priority = 2;
                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined "+JSON.stringify(error));
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), modifyIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_display_name, modifyIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, modifyIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), modifyIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, modifyIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), modifyIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_name, modifyIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, modifyIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, modifyIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, newIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.objRef, newIssue.created_by.user_id, "created_by.user_id should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.objRef, newIssue.reported_by.objRef, "reported_by.objRef should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, newIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.isAbove(issue.status_changed_at, newIssue.status_changed_at, "status_changed_at should should greater than older value when status changes");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });
    });

    it("Issue Update Test : description, reported_by", (done) => {
        "use strict";

        issueLib.getByIssueId(issue_id, (error, issue) => {
            try {
                modifyIssue.issue_id = issue.issue_id;
                modifyIssue.description = faker.lorem.sentences()+" modified";
                modifyIssue.reported_by.user_id = 2;
                modifyIssue.reported_by.user_display_name = faker.name.findName();

                issueLib.update(modifyIssue, (error) => {
                    try {
                        if (error) {
                            done(error);
                        }
                        assert.isUndefined(error, "Error should be undefined "+JSON.stringify(error));
                        issueLib.getByIssueId(issue_id, (error, issue) => {
                            try {
                                assert.isDefined(issue._id, "_id is required");
                                assert.isDefined(issue.issue_id, "issue_id is required");
                                assert.strictEqual(issue.issue_id, issue_id, "issue_id should equal");
                                assert.strictEqual(issue.subject, modifyIssue.subject, "subject should equal");
                                assert.strictEqual(issue.body, modifyIssue.body, "body should equal");
                                assert.isDefined(issue.customer.objRef, "customer.objRef required");
                                assert.strictEqual(issue.customer.objRef.toString(), modifyIssue.customer.objRef.toString(), "customer.objRef should equal");
                                assert.strictEqual(issue.customer.customer_display_name, modifyIssue.customer.customer_display_name, "customer.customer_display_name should equal");
                                assert.strictEqual(issue.channel.channel_type, modifyIssue.channel.channel_type, "channel.channel_type should equal");
                                assert.strictEqual(issue.assigned_user.objRef.toString(), modifyIssue.assigned_user.objRef.toString(), "assigned_user.objRef should equal");
                                assert.strictEqual(issue.assigned_user.user_display_name, modifyIssue.assigned_user.user_display_name, "assigned_user.user_display_name should equal");
                                assert.strictEqual(issue.assigned_group.objRef.toString(), modifyIssue.assigned_group.objRef.toString(), "assigned_group.objRef should equal");
                                assert.strictEqual(issue.assigned_group.group_name, modifyIssue.assigned_group.group_name, "assigned_group.group_name should equal");
                                assert.strictEqual(issue.status, modifyIssue.status, "status should equal");
                                assert.strictEqual(issue.priority, modifyIssue.priority, "priority should equal");
                                assert.strictEqual(issue.description, modifyIssue.description, "description should equal");
                                assert.strictEqual(issue.created_by.objRef, newIssue.created_by.objRef, "created_by.objRef should equal");
                                assert.strictEqual(issue.created_by.user_display_name, newIssue.created_by.user_display_name, "created_by.user_display_name should equal");
                                assert.strictEqual(issue.reported_by.objRef, modifyIssue.reported_by.objRef, "reported_by.objRef should equal");
                                assert.strictEqual(issue.reported_by.user_display_name, modifyIssue.reported_by.user_display_name, "reported_by.user_display_name should equal");
                                assert.isAbove(issue.status_changed_at, newIssue.status_changed_at, "status_changed_at should should greater than older value when status changes");
                                assert.strictEqual(issue.created_at.toString(), newIssue.created_at.toString(), "created_at should equal");
                                assert.deepEqual(issue.labels.toString(), newIssue.labels.toString(), "labels should be equal");
                                done();
                            } catch (exp) {
                                done(exp);
                            }
                        });
                    } catch (exp) {
                        done(exp);
                    }
                });
            } catch (exp) {
                done(exp);
            }

        });

    });


    it("Issue Update Test : labels");
    it("Issue Erase Test");
    it("Issue Create Test: validate while Creating new Issue");
    it("Issue Update Test: validate while Updating existing Issue");



})
;








