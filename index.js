/****
 * Created by Renju on 2/17/2016.
 */
"use strict";
module.exports = {
    issue: require('./src/issue'),
    issueModel: require('./model/issue.model').model
};